// Ignore the path errors: remember that this file is actually copied from a different directory!
import {LoggingLevel} from "../app/utils/logging/logging-level";

export const environment = {
  name: 'production',
  api: {
    server: {
      address: 'https://app.seeld.io'
    }
  },
  features: {
    attachment: false
  },
  logging: {
    consoleOut: true,
    level: LoggingLevel.DEBUG
  },
  messages: {
    polling: {
      fallbackInterval: 30 * 1000, // 30 seconds
      interval: 5 * 60 * 1000 // 5 minutes
    }
  },
  notifications: {
    connectionTimeout: 15 * 1000, // 15 seconds
    reconnectionAttemptDelay: 5 * 60 * 1000, // 5 minutes
    refresh: {
      interval: 10 * 60 * 1000 // 10 minutes
    }
  },
  wsapi: {
    server: {
      address: 'https://app.seeld.io'
    }
  }
};
