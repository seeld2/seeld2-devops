// Ignore the path errors: remember that this file is actually copied from a different directory!
import {LoggingLevel} from "../app/utils/logging/logging-level";

export const environment = {
  name: 'vbox',
  api: {
    server: {
      address: 'https://192.168.56.11'
    }
  },
  features: {
    attachment: false
  },
  logging: {
    consoleOut: true,
    level: LoggingLevel.TRACE
  },
  messages: {
    polling: {
      fallbackInterval: 30 * 1000, // 30 seconds
      interval: 2 * 60 * 1000 // 2 minutes
    }
  },
  notifications: {
    connectionTimeout: 15 * 1000, // 15 seconds
    reconnectionAttemptDelay: 2 * 60 * 1000, // 2 minutes
    refresh: {
      interval: 10 * 60 * 1000 // 10 minutes
    }
  },
  wsapi: {
    server: {
      address: 'https://192.168.56.11'
    }
  }
};
