The command line used to generate the .crt and .key files is:

```shell script
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 9999 -nodes -out seeld-vbox.crt -keyout seeld-vbox.key
```
