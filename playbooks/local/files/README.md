This `config.xml` file is copied over from the front end source code.

It is used as a backup while another `config.ts` file (one that does not allow cleartext exchanges) is copied to the source, just the time to build the front end client.
Once finished the original (backed up) file is then restored, from this folder to the source code.
