This `rfc5054-safe-prime-config.js` file is copied over from the front end source code.

It is used as a backup while another `rfc5054-safe-prime-config` file is copied to the source, just the time to build the front end client.
Once finished the original (backed up) file is then restored, from this folder to the source code.
