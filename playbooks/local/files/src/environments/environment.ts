import {LoggingLevel} from "../app/utils/logging/logging-level";

export const environment = {
  name: 'development',
  api: {
    server: {
      address: 'http://localhost:8080'
    }
  },
  features: {
    attachment: false
  },
  logging: {
    consoleOut: true,
    level: LoggingLevel.TRACE
  },
  messages: {
    polling: { // Polls (queries for) unread messages
      fallbackInterval: 15 * 1000, // 15 seconds / The interval used to poll new messages if the system has issues establishing a websocket connection
      interval: 60 * 1000 // 1 minute / The normal interval at which the poller will query the server for new messages
    }
  },
  notifications: {
    connectionTimeout: 15 * 1000, // 15 seconds / The time after which the system considers the websocket connection attempt has failed
    reconnectionAttemptDelay: 60 * 1000, // 1 minute / In case of web socket error, the system will attempt to reactivate teh connection after this amount of time
    refresh: {
      interval: 5 * 60 * 1000 // 5 minutes / The interval at which the box addresses registered to get websocket notifications are refreshed.
    }
  },
  wsapi: {
    server: {
      address: 'http://localhost:8080'
    }
  }
};
