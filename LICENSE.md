# LICENSE

This project **be.seeld.seeld2-devops** is licensed under a **CC0 - Public Domain Dedication (CC Zero)** (see https://creativecommons.org/publicdomain/zero/1.0/ for details).
