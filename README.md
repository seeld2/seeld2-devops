# seeld2-devops

This module regroups everything needed to manage the configuration of Seeld servers, deploy its various modules and orchestrate those deployments.

# Release notes

## 2.22.0

* Adapt Ionic config to API 33

## 2.21.0

* Increase the normal messages' polling delay for production environment.ts
* Decrease splash screen delay on mobile application
* Update mobile config versions to 2.21.0

## 0.19.0

* Removed the "server" role when running "front.deploy.yaml": it will update the server, and if that's the case the server will reboot, forcing me to reboot the serverapp ("back.reboot.serverapp.yaml")

### 0.18.3

* Update config.no_cleartext.sml version to 0.18.3

### 0.18.2

* Update config.no_cleartext.sml version to 0.18.2

## 0.18.0

* Update config.no_cleartext.sml version to 0.18.0

## 0.17.0

* Update min version to Android Sdk 24
* Add missing ic_stat_seeld.png resource file
* Set splash screen to 2 secs

## 0.16.0

* Create playbooks and config files to deploy seeld2-shredder

### 0.15.1

* Attempt fixing "split-brain" issue by adapting various timeout parameters
* Use "sorted" hosts order for specific playbooks
* Ignore errors when cache-update goes wrong for any reason

## 0.15.0

* Ignore errors when updating apt cache: install should go on even if we deb repositories are failing for some reason

## 0.14.0

* Implement playbooks for packaging and deploying sysreports

## 0.13.0

* Rename old servers.yml playbook to back-and-front.yml

### 0.12.2

* First tests with fail2ban
* Update Ignite to 2.10.0
* Add playbook that only updates servers and restart Seeld server Java apps
* Add server updates when provisioning "db" servers

### 0.12.1

* Change grouping of hosts into front, back and db

## 0.12.0

* Add deployment of JKS for Ignite SSL communication
* Fix bug in ignite's role (was not checking ignite's presence correctly)
* Increase timeout and restart times in seeld2-server.service
* Add StartLimitBurst and StartLimitIntervalSec (restart limitation) for seeld2-server.service

### 0.11.2

* Modify deployment books to regroup back and front roles on one server only
* Update ignite to 2.9.1
* Change path to LoggingLevel following client refactoring

## 0.11.0

* Modify polling and notifications' times
* Update names of application.properties following refactoring

## 0.10.0

* Update import path of LoggingLevel following refactoring in client code

### 0.9.1

* Fix "mode" when copying files remotely: following ansible update, default went from 666 to 600. Restored to 666.
Adapt PWA's environment.ts

## 0.9.0

* Minor updates to config.xml local backup and environment.ts configuration file

### 0.8.1

* Use blocks in playbooks to roll back in case of issues
* Create playbook for Android build

## 0.8.0

* Update applications.properties to separate REST and WebSocket allowed origins

### 0.7.3

* Update allowed origins to include WebView >= 4 origins

### 0.7.2

* Update environment.ts files with additional notifications' configuration values

### 0.7.1

* Update environment values

## 0.7.0

* Add environment values to handle polling and WS registration refresh delays

## 0.6.0

* Add JAVA_HOME to devops playbook
